# A very short introduction to `LaTex`

#### Shehzaman Khatib

---

* `LaTex` pronounced as *LAY-tek* or *LAH-tek*
* Work only on the content, and `LaTex` will take care of formatting
* Designed and mostly written by Donald Knuth in 1978
* [The Comprehensive TeX Archive Network](https://ctan.org/)

---

## Writing a document

+++

Very simple, define paper size, font size and let the defaults do the rest

a4paper, letterpaper - more compicated sizes in `\usepackage{geometery}`

```latex
\documentclass[a4paper,12pt]{article}
\usepackage[utf8]{inputenc}

\title{Latex generic}
\author{Shehzaman Khatib}
\date{\today}

\usepackage{natbib}
\usepackage{graphicx}

```
---

## Writing a paper

+++

Document preamble for Elsevier (Lancet journal)

[Instructions](https://www.elsevier.com/__data/assets/pdf_file/0008/56843/elsdoc-1.pdf)

```latex
\documentclass[preprint,12pt]{elsarticle}

OR

\documentclass[final,5p,times,twocolumn]{elsarticle}

```

+++

Packages for use - math, images, line number... over 5000+ packages on [CTAN](https://ctan.org)

```latex

\usepackage{graphics}
\usepackage{amssymb}
\usepackage{hyperref}

```

+++

Begin article

```latex

\journal{Lancet Journal of St. Johns}

\begin{document}

\begin{frontmatter}

%% Title, authors, addresses, abstract, keywords

\end{frontmatter}

%% Rest of your article

\end{document}

```

+++

Add a section (plus some other commands like line numbering)

```latex
\section{Introduction}
\label{S:1}

Integer risus dui, condimentum et gravida vitae, adipiscing et enim. 
Aliquam erat volutpat. Pellentesque diam sapien, egestas eget gravida ut, 
tempor eu nulla. Vestibulum mollis pretium lacus eget venenatis. 
Fusce gravida nisl quis est molestie eu luctus ipsum pretium. Maecenas non eros lorem, vel adipiscing odio. Etiam dolor risus, mattis in pellentesque id, 
pellentesque eu nibh. Mauris.

Refer this section \ref{S:1}

```

+++

Bullet points or Numbering

```latex
\begin{itemize}
    \item First point
    \item second point
\end{itemize}
```

```latex
\begin{enumerate}
    \item First number
    \item Second number
\end{enumerate}
```

+++

Tables - vertical line, horizontal line, alignment of each column

```latex
\begin{table}[h]
\centering
    \begin{tabular}{l l l}
    \hline
    \textbf{Treatments} & \textbf{Response 1} & \textbf{Response 2}\\
    \hline
    Treatment 1 & 0.0003262 & 0.562 \\
    Treatment 2 & 0.0015681 & 0.910 \\
    Treatment 3 & 0.0009271 & 0.296 \\
    \hline
    \end{tabular}
\caption{Table caption}
\label{tab:table1}
\end{table}
```

+++

Images - export data plots / graphs in `.eps` format.

Use `.png` whenever possible. Avoid `.jpeg` 

```latex
\begin{figure}[h]
\centering
    \includegraphics[width=1\linewidth]{images/test_eps.eps}
\caption{Figure caption}
\end{figure}
```

+++

More complicated figure placement

```latex
\begin{figure}
  \centering
```
+++
```latex
  \begin{subfigure}[t]{0.2\textwidth}
    \includegraphics[width=\linewidth]{images/kitten.png}
    \caption{A kitten}
    \label{fig:kitten}
  \end{subfigure}
  \hspace{\fill}
  \begin{subfigure}[t]{0.2\textwidth}
    \includegraphics[width=\linewidth]{images/placeholder.jpg}
    \caption{A placeholder}
    \label{fig:palce}
  \end{subfigure}
  \hspace{\fill}
  \begin{subfigure}[t]{0.2\textwidth}
    \includegraphics[width=\linewidth]{images/test_eps.eps}
    \caption{An EPS logo}
    \label{fig:logo}
  \end{subfigure}
```
+++  
```latex
  \caption{Pictures of things}
  \label{fig:animals}
\end{figure}
```


+++ 

Mathematics - amsmath package

Equation environment

```latex
\begin{equation}
\label{eq:emc}
e = mc^2
\end{equation}
```

+++

Bibliography

Use give style file

```latex
\bibliographystyle{model1-num-names}
\bibliography{sample.bib}
```
